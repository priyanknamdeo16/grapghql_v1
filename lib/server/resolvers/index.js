"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _connectors = require("../connectors");

var _pubnub = require("../pubnub");

var _pubnub2 = _interopRequireDefault(_pubnub);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  CartItem: {
    product: function product(cart, args, ctx) {
      return (0, _connectors.getProduct)(cart.productId);
    }
  },
  User: {
    assets: function assets(user, args, ctx) {
      var o = (0, _connectors.getAssetsByIds)(user.assets);
      console.log("LL" + o.length);
      return (0, _connectors.getAssetsByIds)(user.assets);
    }
  },
  Query: {
    products: function products(_, args, ctx) {
      return (0, _connectors.getProducts)();
    },
    product: function product(_, args, ctx) {
      return (0, _connectors.getProduct)(args.id);
    },
    cartItem: function cartItem(_, args, ctx) {
      return (0, _connectors.getCartItem)(args.id);
    },
    cartItems: function cartItems(_, args, ctx) {
      return (0, _connectors.getCartItems)();
    },
    assets: function assets(_, args, ctx) {
      return (0, _connectors.getAssets)();
    },
    user: function user(_, args, ctx) {
      return getUserById(args.id);
    },
    users: function users(_, args, ctx) {
      return (0, _connectors.getUsers)();
    }
  },
  Mutation: {
    addProduct: function addProduct(_, args, ctx) {
      return (0, _connectors.addProduct)(args);
    },
    addToCart: function addToCart(_, args, ctx) {
      return (0, _connectors.addToCart)(args);
    },
    deleteCartItem: function deleteCartItem(_, args, ctx) {
      return (0, _connectors.deleteCartItem)(args);
    },
    addAssetToUser: function (_addAssetToUser) {
      function addAssetToUser(_x, _x2, _x3) {
        return _addAssetToUser.apply(this, arguments);
      }

      addAssetToUser.toString = function () {
        return _addAssetToUser.toString();
      };

      return addAssetToUser;
    }(function (_, args, ctx) {
      return addAssetToUser(args);
    })
  },
  Subscription: {
    onNewCartItem: {
      resolve: function resolve(payload) {
        console.log("Subscription", payload);
        return payload;
      },

      subscribe: function subscribe() {
        return _pubnub2.default.asyncIterator("ON_NEW_CART_ITEM");
      }
    }
  }
};