"use strict";

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _apolloServerExpress = require("apollo-server-express");

var _graphql = require("graphql");

var _http = require("http");

var _subscriptionsTransportWs = require("subscriptions-transport-ws");

var _morgan = require("morgan");

var _morgan2 = _interopRequireDefault(_morgan);

var _schema = require("./schema");

var _schema2 = _interopRequireDefault(_schema);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PORT = 8000;

var app = (0, _express2.default)();

app.use((0, _morgan2.default)(function (tokens, req, res) {
  return [tokens.method(req, res), tokens.url(req, res), tokens.status(req, res), tokens.res(req, res, "content-length"), "-", tokens["response-time"](req, res), "ms", JSON.stringify(req.body, null, 2)].join(" ");
}));

app.use(function (req, res, next) {
  setTimeout(next, 500);
});

app.use("/graphql", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");
  if (req.method === "OPTIONS") {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use("/graphql", _bodyParser2.default.json(), (0, _apolloServerExpress.graphqlExpress)({ schema: _schema2.default }));

var server = (0, _http.createServer)(app);

new _subscriptionsTransportWs.SubscriptionServer({ schema: _schema2.default, execute: _graphql.execute, subscribe: _graphql.subscribe }, { server: server, path: "/subscriptions" });

app.use("/graphiql", (0, _apolloServerExpress.graphiqlExpress)({
  endpointURL: "/graphql"
}));

//static content, is served from build directory
app.use(_express2.default.static(__dirname + "../../../build")).get("/", function (req, res) {
  res.sendFile("index.html", {
    root: __dirname + "../../../build"
  });
});

server.listen(PORT, function () {
  console.log("server running on port " + PORT);
});