"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.executableSchema = undefined;

var _graphqlTools = require("graphql-tools");

var _resolvers = require("../resolvers");

var _resolvers2 = _interopRequireDefault(_resolvers);

var _schema = require("./schema");

var _schema2 = _interopRequireDefault(_schema);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var executableSchema = exports.executableSchema = (0, _graphqlTools.makeExecutableSchema)({
  typeDefs: _schema2.default,
  resolvers: _resolvers2.default
});

exports.default = executableSchema;