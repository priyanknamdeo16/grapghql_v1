"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProducts = getProducts;
exports.getProduct = getProduct;
exports.getCartItem = getCartItem;
exports.getCartItems = getCartItems;
exports.addProduct = addProduct;
exports.addToCart = addToCart;
exports.deleteCartItem = deleteCartItem;
exports.getUsers = getUsers;
exports.getAssets = getAssets;
exports.getAssetsByIds = getAssetsByIds;
exports.getAssetById = getAssetById;
exports.getUserById = getUserById;
exports.addAssetToUser = addAssetToUser;
exports.getUserAssetsByUserId = getUserAssetsByUserId;

var _pubnub = require("../pubnub");

var _pubnub2 = _interopRequireDefault(_pubnub);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var products = [{
  id: 1,
  name: "Macbook",
  description: "Latest Macbook with 16GB ram and Quad core processor",
  price: 65000,
  url: "/img/macbook.jpeg"
}, {
  id: 2,
  name: "Keyboard",
  description: "Ergonomic keyboard",
  price: 3000,
  url: "/img/keyboard.jpeg"
}];

var cartItems = [{ id: 1, productId: 1 }];

function getProducts() {
  return products;
}

function getProduct(id) {
  return products.find(function (product) {
    return product.id === id;
  });
}

function getCartItem(id) {
  return cartItems.find(function (c) {
    return c.id === id;
  });
}

function getCartItems() {
  return cartItems;
}

function addProduct(args) {
  if (products.find(function (p) {
    return p.id === args.id;
  })) {
    throw new Error("Duplicate product id");
  }
  var newProduct = { id: args.id, name: args.name, price: args.price };
  products = [].concat(_toConsumableArray(products), [newProduct]);
  return newProduct;
}

function addToCart(args) {
  if (cartItems.find(function (c) {
    return c.productId === args.productId;
  })) {
    throw new Error("Product already in cart");
  }
  var newCartItem = { id: cartItems.length + 1, productId: args.productId };
  cartItems.push(newCartItem);
  _pubnub2.default.publish("ON_NEW_CART_ITEM", newCartItem);
  return newCartItem;
}

function deleteCartItem(args) {
  cartItems = cartItems.filter(function (c) {
    return c.id !== args.id;
  });
  return args.id;
}

///new implementation
var assets = [{
  id: 1,
  name: "samsung smart phone",
  price: 9000,
  currency: "INR"
}, {
  id: 2,
  name: "iPhone",
  price: 10000,
  currency: "INR"
}];

var users = [{
  id: 1,
  firstName: "Priyank",
  lastName: "Namdeo",
  assets: [1, 2]
}, {
  id: 2,
  firstName: "Nitin",
  lastName: "Nema",
  assets: [2]
}];

function getUsers() {
  return users;
}

function getAssets() {
  return assets;
}

function getAssetsByIds(arr) {
  if (!arr) {
    return;
  }

  var map = {};

  for (var i = 0; i < arr.length; i++) {
    map[arr[i]] = arr[i];
  }

  return assets.filter(function (asset) {
    return map.hasOwnProperty(asset.id);
  });
}

function getAssetById(id) {
  return assets.find(function (item) {
    return item.id === id;
  });
}

function getUserById(id) {
  return users.find(function (item) {
    return item.id === id;
  });
}

function addAssetToUser(args) {
  if (args.userId, args.assetId) {
    throw new Error("Asset is already attached to user");
  }
  //return newCartItem;
}

function getUserAssetsByUserId(id) {
  var user = users.find(function (user) {
    return user.id === id;
  });
  var userAssets = [];
  if (user && user.assets) {
    user.assets.map(function (asset) {
      if (asset) {
        userAssets.push(getAssetById(asset.id));
      }
    });
  }
  return userAssets;
}