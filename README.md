# GrapghQL_V1


1. clone repo
2. install dependency

yarn

3. start UI

yarn webpack
yarn webpack-dev-server

UI will be started at
http://localhost:5000/

--now you can change UI code

4. start server

yarn server

Server will be started at 
http://localhost:5000/

5. To see graphiql, use this 

http://localhost:8000/graphiql

e.g. request

{
  user {
    lastName
  }
}


response 

{
  "data": {
    "user": {
      "lastName": "Dow"
    }
  }
}

