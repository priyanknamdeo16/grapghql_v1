//custom webpack-dev-server with HMR
const webpackDevServer = require("webpack-dev-server");
const webpack = require("webpack");

const config = require("./webpack.config.js");
const options = {
  contentBase: "./dist",
  hot: true,
  host: "localhost",
  //   proxy: {
  //     "/graghql": "http://localhost:8000/graghql",
  //     "/graphiql": "http://localhost:8000/graphiql"
  //   }
  proxy: {
    "/graphql": {
      target: "http://localhost:8000"
    },
    "/graphiql": {
      target: "http://localhost:8000"
    },
    "/subscriptions": {
      target: "ws://localhost:8000",
      ws: true
    }
  }
};

webpackDevServer.addDevServerEntrypoints(config, options);

const compiler = webpack(config);

const server = new webpackDevServer(compiler, options);

server.listen(5000, "localhost", () => {
  console.log("dev server listening on port 5000");
});
