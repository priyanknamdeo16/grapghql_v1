var path = require("path");
var HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const webpack = require("webpack");

var config = {
  mode: "development",
  entry: "./src/client/index.tsx",
  devtool: "source-map",
  //given another file
  //   devServer: {
  //     contentBase: "./dist"
  //   },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "awesome-typescript-loader",
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|ttf)$/,
        use: ["file-loader"]
      },
      //{ test: /\.graphql?$/, loader: "webpack-graphql-loader" },
      { test: /\.graphql?$/, loader: "graphql-tag/loader" }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".png", ".jpeg"]
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  plugins: [
    //new CleanWebpackPlugin(["dist"]),
    new HtmlWebpackPlugin({
      template: "./src/client/index.html"
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    })
  ]
};

module.exports = config;
