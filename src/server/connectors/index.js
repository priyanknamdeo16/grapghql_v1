import pubsub from "../pubnub";

let products = [
  {
    id: 1,
    name: "Macbook",
    description: "Latest Macbook with 16GB ram and Quad core processor",
    price: 65000,
    url: "/img/macbook.jpeg"
  },
  {
    id: 2,
    name: "Keyboard",
    description: "Ergonomic keyboard",
    price: 3000,
    url: "/img/keyboard.jpeg"
  }
];

let cartItems = [{ id: 1, productId: 1 }];

export function getProducts() {
  return products;
}

export function getProduct(id) {
  return products.find(product => product.id === id);
}

export function getCartItem(id) {
  return cartItems.find(c => c.id === id);
}

export function getCartItems() {
  return cartItems;
}

export function addProduct(args) {
  if (products.find(p => p.id === args.id)) {
    throw new Error("Duplicate product id");
  }
  const newProduct = { id: args.id, name: args.name, price: args.price };
  products = [...products, newProduct];
  return newProduct;
}

export function addToCart(args) {
  if (cartItems.find(c => c.productId === args.productId)) {
    throw new Error("Product already in cart");
  }
  const newCartItem = { id: cartItems.length + 1, productId: args.productId };
  cartItems.push(newCartItem);
  pubsub.publish("ON_NEW_CART_ITEM", newCartItem);
  return newCartItem;
}

export function deleteCartItem(args) {
  cartItems = cartItems.filter(c => c.id !== args.id);
  return args.id;
}

///new implementation
let assets = [
  {
    id: 1,
    name: "samsung smart phone",
    price: 9000,
    currency: "INR"
  },
  {
    id: 2,
    name: "iPhone",
    price: 10000,
    currency: "INR"
  }
];

let users = [
  {
    id: 1,
    firstName: "Priyank",
    lastName: "Namdeo",
    assets: [1, 2]
  },
  {
    id: 2,
    firstName: "Nitin",
    lastName: "Nema",
    assets: [2]
  }
];

export function getUsers() {
  return users;
}

export function getAssets() {
  return assets;
}

export function getAssetsByIds(arr) {
  if (!arr) {
    return;
  }

  var map = {};

  for (var i = 0; i < arr.length; i++) {
    map[arr[i]] = arr[i];
  }

  return assets.filter(asset => {
    return map.hasOwnProperty(asset.id);
  });
}

export function getAssetById(id) {
  return assets.find(item => item.id === id);
}

export function getUserById(id) {
  return users.find(item => item.id === id);
}

export function addAssetToUser(args) {
  if ((args.userId, args.assetId)) {
    throw new Error("Asset is already attached to user");
  }
  //return newCartItem;
}

export function getUserAssetsByUserId(id) {
  var user = users.find(user => user.id === id);
  var userAssets = [];
  if (user && user.assets) {
    user.assets.map(asset => {
      if (asset) {
        userAssets.push(getAssetById(asset.id));
      }
    });
  }
  return userAssets;
}
