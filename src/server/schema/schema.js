export default `
  type Asset {
     id: Int,
     name : String,
     price : Int,
     currency : String  
  }

  type User {
    id: Int!
    firstName: String
    lastName: String,
    assets : [Asset]
  }

  type Product {
    id: Int!
    name: String
    description: String
    price: Int
    url: String
  }

  type CartItem {
    id: Int!
    product: Product
  }

  type Query {
    assets: [Asset]
    asset(id: Int): Asset
    users: [User],
    user(id: Int): User
    product(id: Int): Product
    products: [Product]
    cartItem(id: Int): CartItem
    cartItems: [CartItem]
  }

  type Mutation {
    deleteCartItem(id: Int): Int
    addProduct(id: Int, name: String, price: Int): Product
    addToCart(productId: Int): CartItem,
    addAssetToUser(userId: Int, assetId : Int) : User
  }

  type Subscription {
    onNewCartItem: CartItem
  }
  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }

`;
