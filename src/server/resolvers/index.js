import {
  getUser,
  getProducts,
  getProduct,
  addProduct,
  getCartItems,
  getCartItem,
  addToCart,
  deleteCartItem,
  getAssets,
  getUsers,
  getUserAssetsByUserId,
  getAssetsByIds
} from "../connectors";
import pubsub from "../pubnub";

export default {
  CartItem: {
    product(cart, args, ctx) {
      return getProduct(cart.productId);
    }
  },
  User: {
    assets(user, args, ctx) {
      var o = getAssetsByIds(user.assets);
      console.log("LL" + o.length);
      return getAssetsByIds(user.assets);
    }
  },
  Query: {
    products(_, args, ctx) {
      return getProducts();
    },
    product(_, args, ctx) {
      return getProduct(args.id);
    },
    cartItem(_, args, ctx) {
      return getCartItem(args.id);
    },
    cartItems(_, args, ctx) {
      return getCartItems();
    },

    assets(_, args, ctx) {
      return getAssets();
    },
    user(_, args, ctx) {
      return getUserById(args.id);
    },
    users(_, args, ctx) {
      return getUsers();
    }
  },
  Mutation: {
    addProduct(_, args, ctx) {
      return addProduct(args);
    },
    addToCart(_, args, ctx) {
      return addToCart(args);
    },
    deleteCartItem(_, args, ctx) {
      return deleteCartItem(args);
    },
    addAssetToUser(_, args, ctx) {
      return addAssetToUser(args);
    }
  },
  Subscription: {
    onNewCartItem: {
      resolve(payload) {
        console.log("Subscription", payload);
        return payload;
      },
      subscribe: () => pubsub.asyncIterator("ON_NEW_CART_ITEM")
    }
  }
};
