import * as React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

/// <reference path="./graphql.d.ts" />
import ASSETS_QUERY from "./graphql/assets.graphql";

export default class Assets extends React.Component<any, any> {
  render() {
    return (
      <Query query={ASSETS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) {
            return <p>Loading...</p>;
          }
          if (error) {
            return <p>Error :(</p>;
          }
          return (
            <div>
              <ul>
                {data.assets.map(item => {
                  return (
                    <li>
                      {item.name} is costing {item.price} {item.currency}
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        }}
      </Query>
    );
  }
}
