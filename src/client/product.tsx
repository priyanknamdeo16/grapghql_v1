import * as React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

/// <reference path="./graphql.d.ts" />
import PRODUCTS_QUERY from "./graphql/product.graphql";

export default class Product extends React.Component<any, any> {
  render() {
    return (
      <Query query={PRODUCTS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) {
            return <p>Loading...</p>;
          }
          if (error) {
            return <p>Error :(</p>;
          }
          return (
            <div>
              <ul>
                {data.products.map(item => {
                  return (
                    <li>
                      <b>Name: {item.name}</b>{" "}
                      <button>Add to cart below</button>
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        }}
      </Query>
    );
  }
}
