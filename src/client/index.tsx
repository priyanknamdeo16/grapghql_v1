import * as React from "react";
import { render } from "react-dom";
import { ApolloProvider, Query } from "react-apollo";

import gql from "graphql-tag";
import User from "./user";

import Assets from "./assets";
import TransferCart from "./transferCart";
import Product from "./product";

import client from "./apollo-client";

const App = () => (
  <ApolloProvider client={client}>
    <div>
      <h2>React + Apollo & Graphql</h2>
      <hr />
      <h4>User and Asset Association</h4>
      <User />
      <hr />
      <h4>All Assets</h4>
      <Assets />
      <hr />
      <h4>All item (other than Asset) that can be added to cart</h4>
      <Product />
      <h4>Itemns that are in cart currently</h4>
      <TransferCart />
    </div>
  </ApolloProvider>
);

render(<App />, document.getElementById("root"));
