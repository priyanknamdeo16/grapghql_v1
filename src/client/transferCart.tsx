import * as React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

/// <reference path="./graphql.d.ts" />
import TRANSFER_CART_QUERY from "./graphql/cart.graphql";

export default class Transfer extends React.Component<any, any> {
  render() {
    return (
      <Query query={TRANSFER_CART_QUERY}>
        {({ loading, error, data }) => {
          if (loading) {
            return <p>Loading...</p>;
          }
          if (error) {
            return <p>Error :(</p>;
          }
          return (
            <div>
              <ul>
                {data.cartItems.map(item => {
                  return (
                    <li>
                      <b>Name: {item.product.name}</b>
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        }}
      </Query>
    );
  }
}
