import ApolloClient from "apollo-boost";
//import { ApolloClient } from "apollo-client";
// import { InMemoryCache } from "apollo-cache-inmemory";
// import { HttpLink } from "apollo-link-http";
// import { ApolloLink, split } from "apollo-link";
// import { withClientState } from "apollo-link-state";
// import { SubscriptionClient } from "subscriptions-transport-ws";
// import { WebSocketLink } from "apollo-link-ws";
// import { getMainDefinition } from "apollo-utilities";
// import { OperationTypeNode, FragmentDefinitionNode, OperationDefinitionNode } from "graphql";

// interface Definintion {
//     kind: string;
//     operation?: string;
//   };
  

// const wsClient = new SubscriptionClient("ws://localhost:8000/subscriptions", {
//   reconnect: true
// });

// // Create a WebSocket link:
// const wsLink = new WebSocketLink(wsClient);

// const cache = new InMemoryCache();

// const stateLink: any = withClientState({
//   cache: cache,
//   resolvers: undefined,
//   defaults: {
//     selectedProducts: []
//   }
// });

// function logLink(operation: any, forward: any) {
//   console.log("log query", operation.query);
//   const obs = forward(operation);
//   if (isSubscriptionQuery({ query: operation.query })) {
//     return obs;
//   }
//   return obs.map((data:any) => {
//     console.log("log data", data);
//     return data;
//   });
// }

// function isSubscriptionQuery({ query } : {query : any}) {
//   const { kind, operation}: Definintion = getMainDefinition(query);
//   return kind === "OperationDefinition" && operation === "subscription";
// }

// const networkLink = split(
//   // split based on operation type
//   isSubscriptionQuery,
//   wsLink,
//   new HttpLink()
// );

// const client = new ApolloClient({
//   cache,
//   link: ApolloLink.from([logLink, stateLink, networkLink])
// });


const client  = new ApolloClient({
    uri: `http://localhost:8000/graphql`
});

export default client;