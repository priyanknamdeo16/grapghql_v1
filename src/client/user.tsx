import * as React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

/// <reference path="./graphql.d.ts" />
import USERS_QUERY from "./graphql/users.graphql";

export default class User extends React.Component<any, any> {
  render() {
    return (
      <Query query={USERS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) {
            return <p>Loading...</p>;
          }
          if (error) {
            return <p>Error :(</p>;
          }
          return (
            <div>
              <ul>
                {data.users.map(user => {
                  return (
                    <li>
                      <b>
                        Name: {user.firstName} {user.lastName}
                      </b>
                      <div>Asset owned </div>
                      <ul>
                        {user.assets.map(asset => {
                          return <li>{asset.name}</li>;
                        })}
                      </ul>
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        }}
      </Query>
    );
  }
}
